//alert("testchart page load");
var dataChart2 = (function() {
  var chartContainer3 = null;
  //var containerToAppendTo = null;
 // var dataSets2 = [];
function createlinechart(chart3, chartContainer3) {
    console.log(chart3);
    lineChart (chart3); 
    }
    function lineChart(chart3) {
      //var chart = new ConvasJS.Chart("chartContainer", {

      var chartconfiguration = {
            animationEnabled: true,
            backgroundColor: "#292e33",
             title: {
              display: true,
              text: 'Overall Payment Experience',
              fontColor: "gray",

            },

            axisY:{
                includeZero: false
            },
            data: [{        
              type: "line",
              color: "green",             
              dataPoints: chart3
            }]
        };
var chart = new CanvasJS.Chart("chartContainer3",chartconfiguration);
//console.log(dataPoints);
chart.render();
}
return {
     createlinechart: function(chart3, chartContainer3) {
      createlinechart(chart3, chartContainer3);  
      } 

  };
})();
